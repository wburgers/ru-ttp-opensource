# ParkinsonNext TTP #
## Installation ##
This TTP works on Wildfly 8 Java servers.
The deployment of the project to the server should be self explanatory.

There are a few more requirements to get this project working.
The TTP makes use of master keys located in the home folder of the user who is running the server.
for example /home/wildfly/TTP.
The key files have the .key extension and contain an AES-256 key in hexadecimal format.
A key can be generated with the following command: 
```
#!bash

openssl enc -aes-256-ecb -k <password> -P -md sha1 -nosalt
```
Where <password> should be replaced with a random password.
The key with the highest number in the filename is used.
So master keys can be updated if ever needed.

Furthermore there is no authentication built in to the application.
I would recommend requiring client certificates that have to be set up in the server.
This way, nobody can make a request to the TTP without client certificates.

## Usage ##
When the TTP is properly set up, and a requesting party has a client certificate, requests can be made.
To request a pseudonym, a requesting party calls the following URL:
```
#!http

https://domain:port/TTP-web/pseudonym

id=test&party=1
```
Parameters are sent as POST parameters.
The pseudonym request accepts the following parameters:
id: The id to be pseudonymized.
party: the party requesting the pseudonym when multiple data providers collect data about a person.

The result of a pseudonym request is a JSON object:

```
#!JSON

{
pseudonym: "uSdfwOFpuLOnZnEOk4Hqew=="
requestingParty: 1
timeSpan: "112014"
keyVersion: 2
}
```


It is also possible to reverse a pseudonym back to the original id.
To do this, the following URL is called:
```
#!http

https://domain:port/TTP-web/reverse_pseudonym

pseudonym=uSdfwOFpuLOnZnEOk4Hqew==&party=1&timespan=112014&keyversion=2
```
Again, the parameters are sent as POST parameters.
The TTP accepts the following paramters:
pseudonym: the pseudonym that should be translated back to an id.
party: the same party that requested the pseudonym.
timespan: the timespan in which this pseudonym is used (december 2014).
keyversion: the identifier of the master key that was used for pseudonym creation.
If any of these values do not match the result of the pseudonym request, the reverse_pseudonym request will fail.
The result of a reverse_pseudonym request is again a JSON object.
```
#!JSON

{
id: "test"
requestingParty: 1
timeSpan: "112014"
}
```
The same id is retrieved.

Please not that there is a current limit of 16 characters for the id value sent to the server.
If parties want to send data larger than 16 characters, another AES mode has to be chosen.
Please use a safe AES mode.